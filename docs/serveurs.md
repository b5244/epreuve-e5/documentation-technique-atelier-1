# Deuxième partie : Mise en place des serveurs et autres machines virtuelles

## 1. Windows Server 2019

Il faut commencer par effectuer l'installation de la machine, ainsi que toutes les mises à jour évidemment.

### 1.1 Paramétrage général

#### 1.1.1 Réseau

L'adressage de cette machine est statique, il faut aller le régler dans les paramètres réseau.

|                       |               |
|-----------------------|---------------|
| Adresse               | 10.2.0.1      |
| Masque de sous réseau | 255.255.255.0 |
| Passerelle par défaut | 10.2.0.254    |

### 1.2 Active Directory

Installer les services Active Directory selon la procédure classique et promouvoir le serveur contrôleur de domaine.
Le nom de la machine doit être ```srvintra.chasseneuil.tierslieux86.fr```

#### 1.2.1 Structucture des OU

TODO script ou v1
TODO script ou v2 pour valorElec

#### 1.2.2 Dossiers partagés

TODO partage personnel tierslieux86
TODO partage adobe acrobat
TODO partages services (script)

#### 1.2.3 GPO

TODO GPOs
TODO audit GPOs

### 1.3 DHCP

L'installation et le paramétrage du DHCP se font en ligne de commande Powershell

Les commandes pour installer la fonction DHCP sont :

```Powershell
Install-WindowsFeature DHCP -IncludeManagementTools
Add-DhcpServerInDC srvintra.chasseneuil.tierslieux86.fr
```

Les plages d'ip à créer sont :

| Nom       | Adresse du réseau | Adresse de début | Adresse de fin | Masque de sous-réseau | Adresses exclues | Passerelle par défaut |
|-----------|-------------------|------------------|----------------|-----------------------|------------------|-----------------------|
| Bureaux   | 192.168.2.0       | 192.168.2.1      | 192.168.2.250  | 255.255.255.0         | aucunes          | 192.168.2.254         |
| Serveurs  | 10.2.0.0          | 10.2.0.1         | 10.2.0.250     | 255.255.255.0         | 10.2.0.1-10      | 10.2.0.254            |
| DMZ       | 172.16.2.0        | 172.16.2.1       | 172.16.2.250   | 255.255.255.0         | 172.16.2.1-10    | 172.16.2.254          |
| Esporting | 172.17.11.0       | 172.17.11.1      | 172.17.11.250  | 255.255.255.0         | aucunes          | 172.17.11.254         |
| Valorelec | 172.17.13.0       | 172.17.13.1      | 172.17.13.250  | 255.255.255.0         | aucunes          | 172.17.13.254         |
| Réunion 1 | 172.17.21.0       | 172.17.21.1      | 172.17.21.250  | 255.255.255.0         | aucunes          | 172.17.21.254         |
| Réunion 2 | 172.17.22.0       | 172.17.22.1      | 172.17.22.250  | 255.255.255.0         | aucunes          | 172.17.22.254         |

Les commandes pour configurer le DHCP sont :

```Powershell
# Pour définir le serveur DNS
Set-DhcpServerv4OptionValue -DnsServer 10.2.0.1 -DnsDomain chasseneuil.tierslieux86.fr

# Pour chaque plage d'ip
Add-DhcpServerv4Scope -name "<nom>" -StartRange <adresse de début> -EndRange <Adresse de fin> -SubnetMask <Masque de sous-réseau> -State Active
Add-DhcpServerv4ExclusionRange -ScopeId <Adresse du réseau> -StartRange <Première adresse d'exclusion> -EndRange <Dernière adresse d'exclusion>
Add-DhcpServerv4OptionValue -ScopeId <Adresse du réseau> -Router <Passerelle par défaut>
```

## 2. Windows 10 Education

Il faut commencer par effectuer l'installation de la machine, ainsi que toutes les mises à jour évidemment.

### 2.1 Paramétrage général

#### 2.1.1 Réseau

L'adressage des postes clients se fait en DHCP, on laissera donc les paramètres par défaut.

## 3. Serveur TrueNas

## 4. Ubuntu server 20.04

### 4.1 Paramétrage général

### 4.2 NTP

### 4.3 TFTP

### 4.4 Syslog

