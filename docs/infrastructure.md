# Première partie : Mise en place de l'infrastructure réseau

## 1. Création physique du réseau

Câbler 2 routeurs et 3 commutateurs selon le schéma suivant :

![img.png](img.png)

Les trois commutateurs sont reliés les uns aux autres afin de mettre en place le RSTP, et les deux commutateurs "Cœur" sont reliés chacun à un routeur.

Chaque liaison est doublée pour faire des agrégats de ports.

Il conviendra également de relier un serveur installé en tant qu'hyperviseur chargé d'émuler sous la forme de machines virtuelles les différentes machines du réseau.

## 2. Configuration commune à tous les équipements réseau

### 2.1 Paramétrage général

TODO Atelier 3 Mission 1 Tâche 1
TODO syslog

### 2.2 Maintenance

#### 2.2.1 Sauvegarde / Restauration de la configuration

#### 2.2.2 Mise à jours des IOS

#### 2.2.3 Réinitialisation d'un mot de passe

### 2.3 Etherchannel

## 3. Configuration des commutateurs

### 3.1 VLANs

### 3.2 VTP

### 3.3 RSTP

## 4. Configuration des routeurs

### 4.1 Sous-interfaces

### 4.2 HSRP 