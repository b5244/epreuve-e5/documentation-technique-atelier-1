# Description du réseau du site de Chasseneuil

## 1. Éléments techniques

Nous allons recenser dans cette partie les équipements réseau existants sur le site

### 1.1. Infrastructure réseau

Les équipements d'infrastructure sont répartis entre plusieurs armoires techniques :
- Une armoire principale au rez-de-chaussée du bâtiment B
- Une armoire de brassage au premier étage du bâtiment B
- Une armoire brassage au premier étage du bâtiment A
- Une armoire de brassage au deuxième étage du bâtiment A

#### 1.1.1 Routeurs

Le routage est assuré par un routeur situé dans l'armoire principale du bâtiment B.
Un pare-feu Stormshield physique ou virtuel est déployé.

#### 1.1.2 Commutateurs

La commutation est assurée par :

- Deux commutateurs-routeurs 24 ports dans l'armoire principale du bâtiment B
- Deux commutateurs 32 ports dans l'armoire du premier étage du bâtiment B
- Un commutateur 26 ports dans l'armoire du premier étage du bâtiment A
- Un commutateur 26 ports dans l'armoire du deuxième étage du bâtiment A

#### 1.1.3 Bornes WIFI
  
Pour le Wifi, il y a 7 bornes en tout :

- Une borne par étage de bureau pour le bâtiment A et le bâtiment B
- Trois bornes au rez-de-chaussée : Salle Libre-service, Salle de réunion B et Salle de formation

### 1.2 Équipements

#### 1.2.1 Serveurs
  
Les différents serveurs déployés au sein de l'ETP :

- Serveur d'annuaire, DNS et DHCP
- Serveur de gestion des configurations et des incidents
- Serveur NAS
- Serveur d'impression
- Serveur debian en DMZ : DNS, serveur web, serveur de messagerie
  
#### 1.2.2 Imprimantes

- Une imprimante dans la salle de formation multimédia
- Une imprimante dans la salle informatique en libre-service
- Dans la salle de reprographie :
- Une photocopieuse noir et blanc 70 pages/minute
- Une imprimante laser couleur A4/A3 25 pages/minute
- Un traceur A2 1 page/minute

#### 1.2.3 Postes fixes

  Les locaux sont pourvus de PC fixes :
- 19 postes dans la salle de formation multimédia
- 24 postes dans la salle informatique libre-service

## 2. Adressage du réseau

| Réseau                        | Adresse        | VLAN |
|-------------------------------|----------------|------|
| Bureaux                       | 192.168.2.0/24 | 2    |
| Serveurs internes             | 10.2.0.0/24    | 10   |
| DMZ                           | 172.16.2.0/24  | 11   |
| Junior entreprise : Esporting | 172.17.11.0/24 | 101  |
| Junior entreprise : ValorElec | 172.17.13.0/24 | 103  |
| Salle de réunion A            | 172.17.21.0/24 | 201  |
| Salle de réunion B            | 172.17.22.0/24 | 202  |

| Équipement                                | Adresse        |
|-------------------------------------------|----------------|
| Serveur DHCP, DNS et Active Directory     | 10.2.0.1       |
| Serveur TrueNas                           | 10.2.0.2       |
| Serveur NTP, Syslog et TFTP               | 10.2.0.5       |
| Routeur LAN                               |                |
| - Sous-interface VLAN 2                   | 192.168.2.254  |
| - Sous-interface VLAN 10                  | 10.2.0.254     |
| - Sous-interface VLAN 11                  | 172.16.2.254   |
| - Sous-interface VLAN 101                 | 172.17.11.254  |
| - Sous-interface VLAN 103                 | 172.17.13.254  |
| - Sous-interface VLAN 201                 | 172.17.21.254  |
| - Sous-interface VLAN 202                 | 172.17.22.254  |
| - Ip publique                             | 192.36.253.254 |
