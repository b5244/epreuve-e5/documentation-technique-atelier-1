# Documentation technique de l'implantation de base du site de Chasseneuil

Cette documentation présente les étapes à réaliser pour mettre en place une maquette du réseau de Chasseneuil ainsi que ses évolutions décrites dans les ateliers "EvolSysWin" et "EvolInfra".

Elle est rédigée dans le cadre des dossiers professionnels pour l'épreuve E5 du BTS SIO option SISR 2022.

Rédacteur : Bruno Michel

# Sommaire

- [Description du réseau existant](docs/description_existant.md)
- [Mise en place de l'infrastructure réseau](docs/infrastructure.md)
- [Evolution du réseau pour la haute disponibilité (atelier 3)](docs/serveurs.md)